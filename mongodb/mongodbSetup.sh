#!/bin/sh

tar -xvf /home/tools/mongodb-linux-x86_64-3.4.1.tgz

mv  /home/tools/mongodb-linux-x86_64-3.4.1 /usr/local/mongodb

cd /usr/local/mongodb/bin/

mkdir -p data/test/logs

mkdir -p data/test/db

cp /home/tools/mongodb.conf .

chmod +x mongdb.conf

./mongod --config mongodb.conf

echo "mongodb started"
